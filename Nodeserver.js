var express = require('express');
var expressHandlebars  = require('express-handlebars');
var path = require('path');
require('./tools/node/node-functions.js');
var app = express();

// Views
app.set('views', path.join(__dirname, './tools/node/views'));
app.engine('handlebars', expressHandlebars({
    defaultLayout: false,
    extname: '.handlebars',
    partialsDir: './tools/node/views/partials/',
    helpers: global.customHandlebarHelpers

}));
app.set('view engine', 'handlebars');

// Routes
var start = require('./tools/node/routes/start');
var modules = require('./tools/node/routes/modules');
var troubleshooting = require('./tools/node/routes/troubleshooting');

// Public
app.use("/public", express.static(path.join(__dirname, './tools/node/public')));

// Requests
app.get('/', function (req, res) {
    res.redirect("/start");
});
app.get('/start', start.index);
app.get('/modules', modules.index);
app.get('/troubleshooting', troubleshooting.index);

// Server start
app.listen(3000, function () {
    console.log('Application started on http://localhost:3000');
});

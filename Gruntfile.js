module.exports = function (grunt) {

    //  ftpush requires a password file named '.ftppass' with content:
    //  {
    //     "key1": {
    //        "username": "storename.jetshop.se",
    //        "password": "PASSWORD"
    //      }
    //  }

    var ftpHost = {
        host: 'files.jetshop.se',
        port: 21,
        authKey: 'key1'
    };
    var ftpExclusion = ['.DS_Store', '.Spotlight-V100', 'ehthumbs.db', 'Thumbs.db', 'support.css'];

    var activeModules = require("./core/modules/modules.json");
    var modules = [];
    modules.sass = [];
    modules.scripts = [];
    modules.views = [];
    for (i=0; i < activeModules.active.length; i++) {
        modules.sass.push("core/modules/" + activeModules.active[i] + "/scss/**.scss");
        modules.scripts.push("core/modules/" + activeModules.active[i] + "/js/**.js");
        modules.views.push("core/modules/" + activeModules.active[i] + "/views/**.hbs");
    }

    grunt.initConfig({
        sass: {
            pkg: grunt.file.readJSON('package.json'),
            options: {
                includePaths: ['core/scss']
            },
            develop: {
                options: {
                    debugInfo: true,
                    trace: true,
                    lineNumbers: true,
                    outputStyle: 'nested',
                    sourceMap: 'stage/css/responsive-base.css.map'
                },
                files: {
                    'stage/css/responsive-base.css': 'core/scss/responsive-base.scss'
                }
            },
            production: {
                options: {
                    outputStyle: 'nested'
                },
                files: {
                    'stage/css/responsive-base.css': 'core/scss/responsive-base.scss'
                }
            }
        },
        ftpush: {
            css: {
                auth: ftpHost,
                src: 'stage/css',
                dest: '/m1/stage/css',
                simple: true,
                exclusions: ftpExclusion,
                keep: []
            },
            scripts: {
                auth: ftpHost,
                src: 'stage/scripts',
                dest: '/m1/stage/scripts',
                simple: true,
                exclusions: ftpExclusion,
                keep: []
            },
            images: {
                auth: ftpHost,
                src: 'stage/images/responsive-base',
                dest: '/m1/stage/images/responsive-base',
                simple: true,
                exclusions: ftpExclusion,
                keep: []
            }
        },
        compress: {
            main: {
                mode: 'zip',
                options: {
                    archive: 'core/package/package-<%= grunt.template.today("yyyy-mm-dd-HH.MM") %>.zip'
                },
                files: [
                    {cwd: 'stage/css', src: ['**', '!support.css'], dest: '/css', expand: true},
                    {cwd: 'stage/images', src: ['**'], dest: '/images', expand: true},
                    {cwd: 'stage/scripts', src: ['**'], dest: '/scripts', expand: true},
                    {cwd: 'core/pages/', src: ['**'], dest: '', expand: true}
                ]
            }
        },
        concat: {
            options: {
                process: function (src, filepath) {
                    return '// \n//    Module: ' + filepath.split('/')[1] + '\n//\n' + src;
                }
            },
            dist: {
                src: modules.scripts,
                dest: 'stage/scripts/responsive-base-modules.js'
            }
        },
        clean: {
            project: {
                src: [
                    "**/.DS_Store",
                    "**/ehthumbs.db",
                    "**/.Spotlight-V100",
                    "**/Thumbs.db"
                ]
            },
            cache: {
                src: [
                    ".grunt/ftpush/**"
                ]
            },
            scripts: {
                src: [
                    ".grunt/ftpush/scripts.json"
                ]
            }
        },
        sass_compile_imports: {
            modules: {
                target: 'core/scss/responsive-base/_modules.scss',
                options: {
                    quiet: true,
                    replacePath: {
                        pattern: 'core/modules/',
                        replace: 'core/modules/'
                    }
                },
                files: [
                    {
                        expand: true,
                        src: modules.sass
                    }
                ]
            }
        },
        handlebars: {
            options: {
                namespace: 'J.views',
                processName: function (filePath) {
                    return filePath.replace(/^views\//, '').replace(/\.hbs$/, '').replace(/^core\/modules\//, '').replace("views/", '');
                }
            },
            production: {
                files: {
                    "stage/scripts/responsive-base-views.js": ["core/views/**/*.hbs", modules.views]
                }
            }
        },
        focus: {
            // grunt-focus will enable targets for grunt-watch
            dist: {
                include: ['sass', 'handlebars', 'modules', 'concat', 'ftpScripts', 'ftpCss', 'ftpImages']
            },
            noftp: {
                include: ['sass', 'handlebars', 'modules', 'concat']
            }
        },
        watch: {
            // grunt-watch are not called, we use grunt-focus as a pre-parser
            sass: {
                files: ['core/scss/*.scss', 'core/scss/responsive-base/*.scss', 'core/scss/foundation/*.scss', 'core/modules/**/scss/**.scss'],
                tasks: ['sass_compile_imports', 'sass:develop']
            },
            ftpScripts: {
                files: ['stage/scripts/*'],
                tasks: ['ftpush:scripts']
            },
            ftpCss: {
                files: ['stage/css/*'],
                tasks: ['ftpush:css']
            },
            ftpImages: {
                files: ['stage/images/responsive-base/*'],
                tasks: ['ftpush:images']
            },
            concat: {
                files: ['core/modules/**/js/**.js'],
                tasks: ['concat']
            },
            handlebars: {
                files: ['core/views/**', 'core/modules/**/views/**.hbs'],
                tasks: ['handlebars']
            },
            modules: {
                files: ['core/modules/modules.json'],
                tasks: ['sass_compile_imports', 'sass:develop', 'handlebars', 'concat']
            }
        }
    });

    // Load all libs
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-sass-compile-imports');
    grunt.loadNpmTasks('grunt-ftpush');
    grunt.loadNpmTasks('grunt-focus');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-compress');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-handlebars');

    // Define tasks
    grunt.registerTask('default', ['sass_compile_imports', 'sass:develop',  'handlebars', 'concat', 'ftpush', 'focus:dist']);
    grunt.registerTask('noftp', ['sass_compile_imports', 'sass:develop', 'concat', 'handlebars', 'focus:noftp']);
    grunt.registerTask('production', ['sass_compile_imports', 'sass:production', 'handlebars', 'ftpush']);
    grunt.registerTask('ftpsync', ['ftpush']);
    grunt.registerTask('package', ['clean:project', 'compress']);
    grunt.registerTask('clean-project', ['clean:project']);
    grunt.registerTask('clear-cache', ['clean:cache']);
    grunt.registerTask('clear-scripts', ['clean:scripts']);
};

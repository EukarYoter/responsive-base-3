var listCategory = {
    init: function () {
        J.translations.push(
            {
                buyButton: {
                    sv: "Köp",
                    en: "Buy",
                    da: "Köp",
                    nb: "Köp",
                    fi: "Köp"
                }
            },
            {
                infoButton: {
                    sv: "Läs mer",
                    en: "Read more",
                    da: "Läs mer",
                    nb: "Läs mer",
                    fi: "Läs mer"
                }
            }
        );
        if (J.data.categoryId === 158 ){
            J.api.category.getProducts(listCategory.renderCategory, 158, true, 1, 158,  true, 6, 1);
        }
        if (J.data.categoryId === 159 ){
            J.api.category.getProducts(listCategory.renderCategory, 159, true, 1, 159,  true, 6, 1);
        }
    },
    renderCategory: function (data, callbackOptions) {
        data.categoryId = callbackOptions;
        var template = J.views['list-category/product-item'];
        var html = template(data);
        $("#list-category-placeholder").append(html);
    }
};

J.pages.addToQueue("product-page", listCategory.init);
// JETPACK MAIN AREA SUBCATEGORIES - VERSION 2.05 ------------------------------------
//
// 2.0  - Initial release for J3
// 2.01 - Added additional symbols to old image name replacement function
// 2.02 - Removed left margin of first subcat in row
//      - Added event for when subcats are rendered
// 2.03 - Add removal of HTML entities from REST results
// 2.04 - Get linked images from first REST call (now possible) = much faster
// 2.05 - Adjusted for Responsive Base


(function () {

    // CART FUNCTION GENERAL VARIABLES ----------------------------------------------------------
    var options = [];
    var fullExecutionAllowed = true;
    var isTouchDevice = J.checker.isTouch;
    var currentCulture = window.JetshopData.Culture;
    var currentCatNumber = window.JetshopData.CategoryId;

    catPageIndicator = "#main-area .category-header-wrapper";
    productIndicator = "#main-area .product-wrapper";
    catContentContainer = "#main-area .category-header-wrapper";
    breadCrumbLink = ".breadcrumb-link";


    // INITIALIZE DETAILED CART  -------------------------------------------------------------------------------------------------------------------------
    function initJetPackMainAreaSubcategories(userOptions) {

        // DEFINE DEFAULT OPTIONS FOR ALL ARGUMENTS
        var defaultOptions = {
            testing: true,
            useLinkedCatImages: true,
            useOldImageName: false,
            customImageSuffix: "jpg",
            renderWithProducts: true,
            subcatsPerRow: 4,
            imageWidth: "114px",
            imageHeight: "90px",
            imageSize: "medium",
            hideCatPageDefaultImage: false
//            containerPadding: "10px 0 10px",
//            containerMargin: "",
//            subcatPadding: "5px",
//            subcatBorder: "1px solid #bbbbbb",
//            subcatHoverBorder: "1px solid #999",
//            subcatBgColor: "#eeeeee",
//            subcatHoverBgColor: "#dddddd",
//            subcatBorderRadius: "5px",
//            subcatBoxShadow: "0 1px 1px rgba(0,0,0,0.1)",
//            subcatHorizontalSpacing: "10px",
//            subcatVerticalSpacing: "10px",
//            subcatNameFontSize: "1em",
//            subcatNameColor: "",
//            subcatNameFontWeight: "normal",
//            subcatNamePadding: "5px 0 0"
        };

        // CHECK FOR DELIVERED ARGUMENTS AND REPLACE DEFAULT OPTIONS WITH THEM
        for (var index in defaultOptions) {
            for (var i in userOptions) {
                if (i == index) {
                    defaultOptions[index] = userOptions[i];
                    break;
                }
            }
        }

        // MAKE OPTIONS INTO MODIFIED DEFAULT OPTIONS
        options = defaultOptions;

        // CHECK FOR TEST CONDITION - IF ONLY PARTIAL EXECUTION IS TO OCCUR
        if (options.testing) {
            if (location.search.match(/test/i)) {
                fullExecutionAllowed = true;
            }
            else {
                fullExecutionAllowed = false;
            }
        }

        // ADD CLASSES FOR CONTENT HIDING
        if (fullExecutionAllowed) {

            // ADD CLASS TO HTML TAG TO INDICATE THAT JETPACK IS ACTIVE
            $("html").addClass("main-area-subcategories-activated");

            // HIDE DEFAULT CATEGORY PAGE IMAGE
            if (options.hideCatPageDefaultImage) {
                $("html").addClass("hide-cat-page-default-image");
            }

            // EXECUTE ON DOCUMENT READY --------------------------------------------
            $(document).ready(function () {

                // ONLY EXECUTE IF REALLY A CATEGORY PAGE
                if ($(catPageIndicator).length) {

                    // ONLY EXECUTE IF OK TO RENDER WITH PRODUCTS (IF THERE ARE ANY)
                    if (!options.renderWithProducts) {
                        if ($(productIndicator).length) {
                            fullExecutionAllowed = false;
                        }
                    }

                    if (fullExecutionAllowed) {
                        // GET SUBCATS FOR CURRENT CATEGORY
                        getSubCats(currentCatNumber);
                    }
                }
                // ELSE DELETE ACTIVATION CLASS
                else {
                    $("html").removeClass("main-area-subcategories-activated");
                }
            });
        }
    }


    // Logging in callbackObject are for testing J.api

    var callbackObject = {
        before: function (callbackOptions) {
//            console.info("Subcategories API before");
//            console.log("callbackOptions:");
//            console.log(callbackOptions);

        },
        success: function(data, callbackOptions, textStatus, jqXHR){
//            console.info("Subcategories API success");
//            console.log("data:");
//            console.log(data);
//            console.log("callbackOptions:");
//            console.log(callbackOptions);
//            console.log("textStatus:");
//            console.log(textStatus);
//            console.log("jqXHR:");
//            console.log(jqXHR);
            render(data, callbackOptions);
        },
        error: function (callbackOptions, xhr, ajaxOptions, thrownError) {
//            console.error("Subcategories API error");
//            console.log("xhr:");
//            console.log(xhr);
//            console.log("ajaxOptions:");
//            console.log(ajaxOptions);
//            console.log("thrownError:");
//            console.log(thrownError);
//            console.log("callbackOptions:");
//            console.log(callbackOptions);
        },
        complete: function (data, callbackOptions, event, xhr) {
//            console.info("Subcategories API complete");
//            console.log("data:");
//            console.log(data);
//            console.log("callbackOptions:");
//            console.log(callbackOptions);
//            console.log("xhr:");
//            console.log(xhr);
//            console.log("event:");
//            console.log(event);
        }
    };

    // GET CART CONTENTS THROUGH WEB SERVICE ------------------------------------------------------------------------------------------------------------------------------------
    function getSubCats(catId) {
        J.api.category.get(callbackObject, {"catId": catId}, true, 1, catId, true);
    }

    function render(data, catId) {
        //TODO: Replace this with a Handlebars view
        var json = typeof data == 'object' ? data : $.parseJSON('(' + data + ')');
        var catData = json[0];

        // ONLY RENDER IF THERE ARE SUBCATEGORIES
        var subcatQty = catData.SubCategories.length;
        if (subcatQty) {

            // DEFINE NO IMAGE URL
            var noImageUrl = "/m1/stage/images/responsive-base/missing-image.png";

            // RENDER SUBCATS
            var subcatCode = "";
            subcatCode += "<div id='main-subcat-container'>";
            subcatCode += "";
            var subcatCounter = 0;
            var subcatsInRowCounter = 0;

            $.each(catData.SubCategories, function (index, subCat) {

                var thisCatId = subCat.Id;
                var thisCatName = subCat.Name;
                var thisCatUrl = subCat.CategoryUrl;
                var thisLinkedImageName = subCat.ImageUrl.Url;
                var thisCustomImageName = thisCatId + "." + options.customImageSuffix;
                var thisOldImageName = catId + "_" + makeOldImageName(thisCatName) + ".jpg";
                var thisImageUrl;

                // UNLINKED CAT IMAGES
                if (options.useLinkedCatImages) {
                    if (thisLinkedImageName) {
                        thisImageUrl = "/pub_images/" + options.imageSize + "/" + thisLinkedImageName;
                    }
                    else {
                        thisImageUrl = noImageUrl;
                    }
                }
                else {
                    if (options.useOldImageName) {
                        thisImageUrl = "/pub_docs/files/category-images/" + thisOldImageName;
                    }
                    else {
                        thisImageUrl = "/pub_docs/files/subcategory-images/" + thisCustomImageName;
                    }
                }

                // INSERT ROW CONTAINER
                if (subcatsInRowCounter == 0) {
                    subcatCode += "<div class='row'>";
                }

                subcatCode += "<a class='subcat' href='" + thisCatUrl + "'>";
                subcatCode += "<span class='image'>";
                subcatCode += "<img data-url='" + thisImageUrl + "' alt='" + thisCatName + "' />";
                subcatCode += "</span>";
                subcatCode += "<span class='name'>" + thisCatName + "</span>";
                subcatCode += "</a>";
                subcatCode += "";

                subcatCounter++;
                subcatsInRowCounter++;

                // END ROW CONTAINER
                if (subcatsInRowCounter >= options.subcatsPerRow || subcatCounter >= subcatQty) {
                    subcatCode += "<div class='clear' style=\"clear:both;\"></div></div>";
                    subcatsInRowCounter = 0;
                }
            });
            subcatCode += "</div>";

            $(catContentContainer).after(subcatCode);

            // BIND ERROR TO IMAGES BEFORE SETTING URL
            $("#main-subcat-container img").error(function () {
                $(this).unbind("error").attr("src", noImageUrl);
            });

            // SET IMAGE URLS
            $("#main-subcat-container img").each(function () {
                var img = $(this);
                var dataUrl = img.attr("data-url");
                $(this).attr("src", $(this).attr("data-url"));
            });

            // SET NAME HEIGHTS FOR ROWS
            $("#main-subcat-container .row").each(function () {
                var highest = 0;
                $(this).find(".name").each(function () {
                    var thisHeight = $(this).height();
                    if (thisHeight > highest) {
                        highest = thisHeight;
                    }
                });
                $(this).find(".name").height(highest);
            });

            // FIRE READY EVENT FOR EVENTUAL CUSTOMIZED STORE SCRIPTS
            if (typeof window.mainAreaSubcategoriesReady == "function") {
                window.mainAreaSubcategoriesReady();
            }
        }
    }


    // MAKE OLD STYLE IMAGE NAME FROM CATEGORY
    function makeOldImageName(name) {
        var newName;
        newName = name.replace(/&#229;|&#228;|&#197;|&#196;|&#198;|&#230;/g, "a").replace(/&#246;|&#214;|&#248;|&#216;/g, "o").replace(/&#232;|&#233;|&#234;|&#200;|&#201;|&#202;/g, "e").replace(/&[^;]*;/g, ""); // LAST ONE IS FOR ALL OTHER HTML ENTITIES
        newName = newName.toLowerCase().replace(/ä|å|æ|á|à|ã/ig, "a").replace(/é|è|ë|ê/ig, "e").replace(/ö|ô|ø/ig, "o").replace(/\W/g, "").replace(/_/ig, "");
        return newName;
    }

    // EXPOSE MAIN FUNCTION TO GLOBAL SCOPE
    window["initJetPackMainAreaSubcategories"] = initJetPackMainAreaSubcategories;

})();


<!-- BEGIN MAIN AREA SUBCATEGORIES JETPACK ----------------->
initJetPackMainAreaSubcategories({
    testing: false,
    useLinkedCatImages: true,
    useOldImageName: false,
    customImageSuffix: "jpg",
    renderWithProducts: true,
    useHorizontalAdvanced: false,
    subcatsPerRow: 1, // Always 1, do not change!
    imageWidth: "114px",
    imageHeight: "90px",
    imageSize: "medium"
});
<!-- END MAIN AREA SUBCATEGORIES JETPACK ----------------->




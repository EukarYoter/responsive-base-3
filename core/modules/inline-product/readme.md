Inline product view for category pages. 

**Limitations:** 

- Do not work if there's a "Load more" button that will add products with a REST-call


**Todo:**

- When displaying products that lack images something weird happens, check this: http://foundation2.jetshop.se/stage/accessories - product "Belt light" (an extra ``#inline-product-placeholder`` are introduced)
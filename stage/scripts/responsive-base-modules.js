// 
//    Module: modules
//
var dynamicCart = {
    //TODO animation on delete item? Async, happening too fast?
    settings: {
        type: "modal", // "modal" or "dropdown"
        showTotalVat: true,
        topDistance: 100,
        topDistanceSmall: 30
    },

    lastProductAdded: 0,

    dcWrapper: false,

    render: function () {
        // Save the ID for last product added to cart, used for animation
        J.cart.lastProductAdded = dynamicCart.lastProductAdded;
        // Items
        var itemTemplate = J.views['dynamic-cart/dynamic-cart-item'];
        var itemHtml = itemTemplate(J.cart);
        $("#dc-content").html(itemHtml);
        // Totals
        var totalSumData = {
            TotalProductSumText: J.cart.TotalProductSumText,
            TotalProductSumNoVatText: J.cart.TotalProductSumNoVatText,
            TotalProductVatSumText: J.cart.TotalProductVatSumText,
            showTotalVat: dynamicCart.settings.showTotalVat
        };
        var totalsTemplate = J.views['dynamic-cart/dynamic-cart-totals'];
        var totalsHtml = totalsTemplate(totalSumData);
        $("#dc-totals").html(totalsHtml);
        dynamicCart.dcWrapper.removeClass("loading").removeClass("deleting");
        setTimeout(function(){
            $(".dc-item-row").removeClass("product-added"); // This class is added in view to highlight last product added
        }, 300);
    },

    open: function (isCartAddOpening) {
        if (isCartAddOpening) {
            dynamicCart.dcWrapper.addClass("loading");
        }
        if (dynamicCart.settings.type === "modal" || Foundation.utils.is_small_only()) {
            if (Foundation.utils.is_small_only()) {
                dynamicCart.dcWrapper.data().cssTop = dynamicCart.settings.topDistanceSmall;
            } else {
                dynamicCart.dcWrapper.data().cssTop = dynamicCart.settings.topDistance;
            }
            dynamicCart.dcWrapper.foundation("reveal", "open");
        }
        else if (dynamicCart.settings.type === "dropdown") {
            dynamicCart.dcWrapper.addClass("dropdown-open").find("#dc-inner").stop(true, true).slideDown("fast");
        }
    },

    close: function () {
        dynamicCart.dcWrapper.removeClass("loading");
        dynamicCart.dcWrapper.foundation("reveal", "close"); // Always close modal
        if (dynamicCart.settings.type === "dropdown") {
            dynamicCart.dcWrapper.removeClass("dropdown-open").find("#dc-inner").stop(true, true).slideUp("fast");
        }
    },

    bind: function () {
        // Bind reveal closed to purge style attributes from cart wrapper
        $(document).on("closed.fndtn.reveal", '[data-reveal]', function () {
            var modal = $(this);
            modal.removeAttr("style");
        });
        // Bind cart click
        $(".small-cart-body").click(function () {
            if (dynamicCart.settings.type === "modal") {
                dynamicCart.open(false);
            }
            else if (dynamicCart.settings.type === "dropdown") {
                if (dynamicCart.dcWrapper.hasClass("dropdown-open")) {
                    dynamicCart.close();
                }
                else {
                    dynamicCart.open();
                }
            }
        });
        // Bind continue button
        $("#dc-continue-btn").click(function (event) {
            event.preventDefault();
            dynamicCart.close();
        });
        // Bind delete button
        dynamicCart.dcWrapper.on("click", ".delete i", function (event) {
            event.preventDefault();
            var clickedBtn = $(this);
            var prodRow = clickedBtn.closest(".dc-item-row");
            var prodRowHeight = prodRow.height();
            var recId = prodRow.attr("data-rec-id");
            prodRow.addClass("deleting").slideUp(300, "easeInOutCubic");
            dynamicCart.dcWrapper.addClass("deleting");
            J.components.deleteFromCart(recId);
        });
    },

    init: function () {
        J.translations.push(
            {
                dcProdLoadMsg: {
                    sv: "Lägger varan i kundvagnen...",
                    nb: "Legger vare i handlekurven...",
                    da: "Lægger varen i indkøbsvognen...",
                    fi: "Tuote lisätään ostoskoriin...",
                    de: "Artikel zum Warenkorb hinzufügen...",
                    en: "Adding item to cart..."
                },
                dcCartLoadMsg: {
                    sv: "Hämtar kundvagnen...",
                    nb: "Henter handlekurven...",
                    da: "Henter indkøbsvogn...",
                    fi: "Ostoskorille siirrytään...",
                    de: "Zum Warenkorb...",
                    en: "Getting cart..."
                },
                dcContinueBtnText: {
                    sv: "Fortsätt handla",
                    nb: "Handle mer",
                    da: "Fortsæt med at shoppe",
                    fi: "Jatka ostoksia",
                    de: "Mit dem Einkauf fortfahren",
                    en: "Continue shopping"
                },
                dcCheckoutBtnText: {
                    sv: "Till kassan",
                    nb: "Gå til kassen",
                    da: "Gå til kassen",
                    fi: "Mene kassalle",
                    de: "Zur Kasse",
                    en: "Proceed to checkout"
                },
                dcCartHeader: {
                    sv: "Din kundvagn",
                    nb: "Din handlekurv",
                    da: "Din kundevogn",
                    fi: "Ostoskorisi",
                    de: "Ihr Warenkorb",
                    en: "Your shopping cart"
                },
                dcItem: {
                    sv: "Artikel",
                    nb: "Artikkel",
                    da: "Vare",
                    fi: "Tuote",
                    de: "Artikel",
                    en: "Article"
                },
                dcPrice: {
                    sv: "Pris",
                    nb: "Pris",
                    da: "Pris",
                    fi: "Hinta",
                    de: "Preis",
                    en: "Price"
                },
                dcQty: {
                    sv: "Antal",
                    nb: "Antall",
                    da: "Antal",
                    fi: "Määrä",
                    de: "Anzahl",
                    en: "Qty"
                },
                dcTotal: {
                    sv: "Summa",
                    nb: "Summer",
                    da: "I alt",
                    fi: "Summa",
                    de: "Summe",
                    en: "Total"
                },
                dcItemTotal: {
                    sv: "Summa artiklar",
                    nb: "Summer artikler",
                    da: "Varer i alt",
                    fi: "Yhteensä tuotteita",
                    de: "Summe Artikel",
                    en: "Total items"
                },
                dcOfWhichVat: {
                    sv: "varav moms",
                    nb: "herav mva",
                    da: "heraf moms",
                    fi: "josta alv:n osuus",
                    de: "davon MwSt",
                    en: "of which VAT"
                },
                dcPlusVat: {
                    sv: "moms tillkommer med",
                    nb: "+ moms",
                    da: "der tillægges moms på",
                    fi: "hinta sisältää alv:n",
                    de: "+ MwSt",
                    en: "+ VAT"
                },
                dcItemNumber: {
                    sv: "Artikelnummer",
                    nb: "Artikkelnummer",
                    da: "Varenummer",
                    fi: "Tuotenumero",
                    de: "Artikelnummer",
                    en: "Item number"
                },
                dcViewCart: {
                    sv: "Se kundvagn",
                    nb: "Se handlekurv",
                    da: "Se indkøbsvogn",
                    fi: "Näytä ostoskori",
                    de: "Warenkorb anzeigen",
                    en: "View cart"
                }
            }
        );

        // Render cart container if not already rendered
        if (!$("#dc-wrapper").length) {
            var template = J.views['dynamic-cart/dynamic-cart'];
            var html = template(null);
            $(".cart-area-wrapper").append(html);
        }
        dynamicCart.dcWrapper = $("#dc-wrapper");

        // Set type as html class
        if (dynamicCart.settings.type === "modal") {
            $("html").addClass("dynamic-cart-modal");
        }
        else if (dynamicCart.settings.type === "dropdown") {
            $("html").addClass("dynamic-cart-dropdown");
        }

        // Bind
        dynamicCart.bind();

        // Hijack add to cart function
        var oldAddToCart = JetShop.StoreControls.Services.General.AddCartItem;
        JetShop.StoreControls.Services.General.AddCartItem = function () {
            console.log(arguments[0].ProductID);
            dynamicCart.lastProductAdded = arguments[0].ProductID;
            dynamicCart.open(true);
            return oldAddToCart.apply(this, arguments);
        };

        J.switch.addToMediumUp(function () {
            if (dynamicCart.settings.type === "dropdown") {
                dynamicCart.close();
                dynamicCart.dcWrapper.removeAttr("style");
            }
        });
        J.switch.addToSmall(function () {
            if (dynamicCart.settings.type === "dropdown") {
                dynamicCart.close();
            }
        });

        $(window).on("cart-updated", function () {
            dynamicCart.render();
        });
    }
};

J.pages.addToQueue("all-pages", dynamicCart.init);

// 
//    Module: modules
//
var catNav = {
    initTopMenu: function () {

        var catNav = $("#cat-nav");
        var navBar = catNav.children(".nav-bar");

        // TOP MENU STYLE
        catNav.addClass("style-vertical-right");
        catNav.find("ul.lv3").wrap("<div class='positioner'></div>");

        // MAKE SLIGHT DELAY ON HOVER
        var hoverAction;
        catNav.on("mouseenter", function () {
            clearTimeout(hoverAction);
            hoverAction = setTimeout(function () {
                catNav.addClass("hovered");
            }, 100);
        }).on("mouseleave", function () {
            clearTimeout(hoverAction);
            catNav.removeClass("hovered");
            catNav.checkTopMenuLayout();
        });

        // ADD TOUCH SUPPORT FOR DESKTOP
        if (J.checker.isTouch && Foundation.utils.is_large_up()) {

            var expandingElements = $("#cat-nav li.lv1.has-subcategories > a, #cat-nav li.lv2.has-subcategories > a");
            expandingElements.click(function (event) {
                var clickedNodeLink = $(this);
                event.preventDefault();

                if (!clickedNodeLink.parent().hasClass("clicked-once")) {
                    $("#cat-nav .clicked-once").not(clickedNodeLink.closest(".lv1")).removeClass("clicked-once");
                    $(this).parent().addClass("clicked-once");
                }
                else {
                    location.href = $(this).attr("href");
                }
            });

            $("body").on("click", function (event) {
                var target = $(event.target);
                if ($("#cat-nav .clicked-once").length && !target.closest("#cat-nav").length) {
                    $("#cat-nav .clicked-once").removeClass("clicked-once");
                }
            });
        }

        // CHECK LV 2 POS SO IT DOES NOT RENDER OUTSIDE OF SCREEN
        // FOR APPLICABLE TOP MENU TYPES
        catNav.checkLv2HorizontalPosition = function () {
            if (Foundation.utils.is_large_up()) {
                catNav.addClass("resizing");
                var windowWidth = $(window).width();
                // LOOP THROUGH LV1 TO FIND LV2 CONTAINERS THAT ARE OUTSIDE VISIBLE WINDOW
                catNav.find(".lv1.has-subcategories").each(function () {
                    var lv1Container = $(this);
                    var lv1OffsetLeft = lv1Container.offset().left;
                    var lv2Container = lv1Container.find("ul.lv2");
                    var lv2Width = lv2Container.outerWidth();
                    var overflow = windowWidth - lv1OffsetLeft - lv2Width;
                    if (overflow < 0) {
                        lv2Container.addClass("moved-horizontally").css("left", (overflow - 10));
                    }
                    else {
                        lv2Container.removeClass("moved-horizontally").css("left", "0px");
                        // IF VERTICAL-RIGHT ALSO CHECK FOR LV3 OUTSIDERS
                        var lv3Outside = false;
                        var lv3Containers = lv1Container.find("ul.lv3");
                        lv3Containers.each(function () {
                            var lv3Container = $(this);
                            var lv3Width = lv3Container.outerWidth();
                            //log(lv3Width);
                            if ((overflow - lv3Width) < 0) {
                                lv3Outside = true;
                                return false;
                            }
                        });
                        if (lv3Outside) {
                            lv2Container.addClass("lv3-to-left");
                        }
                        else {
                            lv2Container.removeClass("lv3-to-left");
                        }
                    }
                });
                catNav.removeClass("resizing");
            }
        };

        // STOP CLICK PROPAGATION FOR LINKS
        catNav.on("click", "a", function (event) {
            event.stopPropagation();
        });

        // TOP CAT MENU SPACE CALCULATION
        var lastWindowHeight = 0;
        catNav.checkTopMenuLayout = function () {

            if (Foundation.utils.is_large_up()) {
                var winScrollTop = $(window).scrollTop();
                var catNavTopOffset = catNav.offset().top; // HEIGHT UNTIL TOP EDGE OF HOVER DYNAMIC CAT MENU CONTAINER
                var navBarHeight = navBar.height(); // HEIGHT OF TOP MENU BAR
                // CHECK IF TOP MENU BAR SHOULD BE FIXED
                if (winScrollTop > (catNavTopOffset + navBarHeight)) { // IS SCROLLED
                    if (!catNav.is(".hovered")) {
                        catNav.height(navBarHeight);
                        $("html").addClass("menu-scrolled");
                        setTimeout(function () {
                            $("html").removeClass("menu-static");
                        }, 10);
                    }
                }
                else { // RETURN TO NORMAL
                    $("html").removeClass("menu-scrolled");
                    catNav.css("height", "auto");
                    setTimeout(function () {
                        $("html").addClass("menu-static");
                    }, 10);
                }
            }
        };

        // CHECK MENU LAYOUT
        catNav.checkTopMenuLayout();
        catNav.checkLv2HorizontalPosition();

        // BIND DYNAMIC MENU SPACE CALC TO RESIZE
        var resizeAction;
        $(window).resize(function () {
            clearTimeout(resizeAction);
            resizeAction = setTimeout(function () {

                // RESIZE DESKTOP MENU
                catNav.checkLv2HorizontalPosition();
                catNav.checkTopMenuLayout();
            }, 100);
        });

        var scrollAction;
        $(window).scroll(function () {
            clearTimeout(scrollAction);
            scrollAction = setTimeout(function () {

                // RESIZE DESKTOP MENU
                catNav.checkTopMenuLayout();
            }, 100);
        });

        // THIS RESETS MENU WHEN GOING FROM MEDIUM TO LARGE
        // FIRED FROM J.switch.toLargeUp
        catNav.resetMobileMenuStyles = function () {
            catNav.add(navBar).removeAttr("style");
        };

        // ADD CHECK FOR MOBILE STYLING WHEN IN LARGE-UP
        J.switch.addToLargeUp(catNav.resetMobileMenuStyles);

    }
};
J.pages.addToQueue("all-pages", catNav.initTopMenu);
// 
//    Module: modules
//
// INIT MOBILE MENU -------------
var catNavMobile = {

    settings: {
        autoCloseInactiveMobileNodes: true
    },

    checkMobileMenuLayout: function (isOpening) {
        var catNav = $("#cat-nav");
        if (!Foundation.utils.is_large_up()) {
            var winScrollTop = $(window).scrollTop();
            var catNavTopOffset = catNav.offset().top; // HEIGHT UNTIL TOP EDGE OF HOVER DYNAMIC CAT MENU CONTAINER
            var navBar = catNav.children(".nav-bar");
            var navBarTopOffset = navBar.offset().top;
            var navBarHeight = navBar.height(); // HEIGHT OF TOP MENU BAR
            var viewportHeight = $(window).height();
            var menuWrapper = $("#menu-wrapper");
            var menuHeight = menuWrapper.height();
            var menuTopOffset;
            // CHECK IF TOP MENU BAR SHOULD BE FIXED
            if (winScrollTop > (catNavTopOffset - menuHeight)) { // IS SCROLLED
                catNav.height(menuHeight);
                $("html").addClass("menu-scrolled");
                setTimeout(function () {
                    $("html").removeClass("menu-static");
                }, 10);
            }
            // RETURN TO NORMAL
            else {
                catNav.css("height", "auto");
                $("html").removeClass("menu-scrolled");
                setTimeout(function () {
                    $("html").addClass("menu-static");
                }, 10);
            }
            // SCROLL CAT NAVIGATION (TIMEOUT TO WAIT FOR DOM CHANGES)
            if ($("html.menu-open").length || isOpening) {
                setTimeout(function () {
                    // TAKE FIXED/NOT FIXED STATE INTO ACCOUNT
                    if ($("html.menu-scrolled").length) {
                        menuTopOffset = winScrollTop;
                    }
                    else {
                        menuTopOffset = menuWrapper.offset().top;
                    }
                    // IF LEFT AREA TOP BELOW CURRENT SCROLL POSITION - MOVE UP
                    if (navBarTopOffset > (menuTopOffset + menuHeight)) {
                        navBar.css("top", (menuTopOffset + menuHeight + 10) + "px");
                    }
                    // IF LEFT AREA TOP ABOVE SCROLL POSITION AND THERE IS SPACE ENOUGH TO FIT IT, MOVE DOWN
                    else if ((viewportHeight - menuHeight) > navBarHeight) {
                        navBar.css("top", (menuTopOffset + menuHeight + 10) + "px");
                    }
                    // IF END OF LONG LEFT AREA ABOVE BOTTOM OF PAGE - MOVE DOWN
                    else {
                        var posDiff = (winScrollTop + viewportHeight) - (navBarTopOffset + navBarHeight);
                        if (posDiff > 150) {
                            navBar.css("top", (navBarTopOffset + posDiff - 10) + "px");
                        }
                    }
                }, 5);
            }
        }
    },

    init: function () {
        var catNav = $("#cat-nav");
        // EXPANDING CLICKS IN MEDIUM-DOWN
        catNav.on("click", "ul, li", function (event) {
            if (!Foundation.utils.is_large_up()) {
                event.stopPropagation();
                var clickedNode = $(this);
                if (clickedNode.is("li.has-subcategories")) {
                    clickedNode.toggleClass("open");
                    if (catNavMobile.settings.autoCloseInactiveMobileNodes) {
                        // CLOSE ALL OPEN NODES NOT DIRECT PARENT OF CLICKED NODE
                        catNav.find(".open").not(clickedNode).not(clickedNode.parentsUntil("#cat-nav")).removeClass("open");
                    }
                    else {
                        // ONLY CLOSE OPEN NODES THAT ARE DIRECT DESCENDANTS OF CLOSED NODE
                        clickedNode.find(".open").removeClass("open");
                    }
                }
            }
        });
        // STOP CLICK PROPAGATION FOR LINKS
        catNav.on("click", "a", function (event) {
            event.stopPropagation();
        });
        // BIND DYNAMIC MENU SPACE CALC TO RESIZE AND SCROLL
        var resizeAction;
        $(window).resize(function () {
            clearTimeout(resizeAction);
            resizeAction = setTimeout(function () {
                catNavMobile.checkMobileMenuLayout();
            }, 100);
        });
        var scrollAction;
        $(window).scroll(function () {
            clearTimeout(scrollAction);
            scrollAction = setTimeout(function () {
                catNavMobile.checkMobileMenuLayout();
            }, 100);
        });
    }
};

J.pages.addToQueue("all-pages", catNavMobile.init);

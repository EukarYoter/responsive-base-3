this["J"] = this["J"] || {};
this["J"]["views"] = this["J"]["views"] || {};

this["J"]["views"]["dynamic-cart/dynamic-cart-item"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"dc-item-row "
    + ((stack1 = (helpers.ifValue || (depth0 && depth0.ifValue) || alias2).call(alias1,(depth0 != null ? depth0.ProductId : depth0),{"name":"ifValue","hash":{"equals":(depths[1] != null ? depths[1].lastProductAdded : depths[1])},"fn":container.program(2, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\" data-id=\""
    + alias4(((helper = (helper = helpers.ProductId || (depth0 != null ? depth0.ProductId : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"ProductId","hash":{},"data":data}) : helper)))
    + "\" data-rec-id=\""
    + alias4(((helper = (helper = helpers.RecId || (depth0 != null ? depth0.RecId : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"RecId","hash":{},"data":data}) : helper)))
    + "\" data-attribute-id=\"-1\">\n    <div class=\"img\">\n"
    + ((stack1 = helpers["if"].call(alias1,((stack1 = ((stack1 = (depth0 != null ? depth0.Images : depth0)) != null ? stack1["0"] : stack1)) != null ? stack1.Url : stack1),{"name":"if","hash":{},"fn":container.program(4, data, 0, blockParams, depths),"inverse":container.program(7, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "")
    + "    </div>\n    <div class=\"item\">\n        <div class=\"description\">\n            <a class=\"name\" href=\""
    + ((stack1 = helpers["if"].call(alias1,(helpers.isStage || (depth0 && depth0.isStage) || alias2).call(alias1,{"name":"isStage","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(5, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + alias4(((helper = (helper = helpers.ProductLink || (depth0 != null ? depth0.ProductLink : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"ProductLink","hash":{},"data":data}) : helper)))
    + "\">"
    + alias4(((helper = (helper = helpers.Name || (depth0 != null ? depth0.Name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"Name","hash":{},"data":data}) : helper)))
    + "</a>\n            <span class=\"item-number\">"
    + alias4((helpers.translate || (depth0 && depth0.translate) || alias2).call(alias1,"dcItemNumber",{"name":"translate","hash":{},"data":data}))
    + ": "
    + alias4(((helper = (helper = helpers.Articlenumber || (depth0 != null ? depth0.Articlenumber : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"Articlenumber","hash":{},"data":data}) : helper)))
    + "</span>\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.Comments : depth0),{"name":"if","hash":{},"fn":container.program(9, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "        </div>\n    </div>\n    <div class=\"price\">\n        <span class=\"price-string\">\n"
    + ((stack1 = helpers["if"].call(alias1,(helpers.isVatIncluded || (depth0 && depth0.isVatIncluded) || alias2).call(alias1,{"name":"isVatIncluded","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(12, data, 0, blockParams, depths),"inverse":container.program(14, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "")
    + "        </span>\n        <span class=\"separator\"> / </span>\n        <span class=\"qty-suffix\">"
    + alias4(((helper = (helper = helpers.QuantitySuffix || (depth0 != null ? depth0.QuantitySuffix : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"QuantitySuffix","hash":{},"data":data}) : helper)))
    + "</span>\n    </div>\n    <div class=\"qty\">\n        <span class=\"label\">"
    + alias4((helpers.translate || (depth0 && depth0.translate) || alias2).call(alias1,"dcQty",{"name":"translate","hash":{},"data":data}))
    + ": </span>\n        <span class=\"value\">"
    + alias4(((helper = (helper = helpers.Quantity || (depth0 != null ? depth0.Quantity : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"Quantity","hash":{},"data":data}) : helper)))
    + "</span>\n    </div>\n    <div class=\"delete\">\n        <i class=\"fa fa-times-circle\"></i>\n    </div>\n    <div class=\"total\">\n        <span>\n"
    + ((stack1 = helpers["if"].call(alias1,(helpers.isVatIncluded || (depth0 && depth0.isVatIncluded) || alias2).call(alias1,{"name":"isVatIncluded","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(16, data, 0, blockParams, depths),"inverse":container.program(18, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "")
    + "        </span>\n    </div>\n</div>\n";
},"2":function(container,depth0,helpers,partials,data) {
    return "product-added";
},"4":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "        <a href=\""
    + ((stack1 = helpers["if"].call(alias1,(helpers.isStage || (depth0 && depth0.isStage) || alias2).call(alias1,{"name":"isStage","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(5, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + alias4(((helper = (helper = helpers.ProductLink || (depth0 != null ? depth0.ProductLink : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"ProductLink","hash":{},"data":data}) : helper)))
    + "\"><img src=\""
    + alias4(container.lambda(((stack1 = ((stack1 = (depth0 != null ? depth0.Images : depth0)) != null ? stack1["0"] : stack1)) != null ? stack1.Url : stack1), depth0))
    + "\" alt=\""
    + alias4(((helper = (helper = helpers.Name || (depth0 != null ? depth0.Name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"Name","hash":{},"data":data}) : helper)))
    + "\"></a>\n";
},"5":function(container,depth0,helpers,partials,data) {
    return "/stage";
},"7":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "        <a href=\""
    + ((stack1 = helpers["if"].call(alias1,(helpers.isStage || (depth0 && depth0.isStage) || alias2).call(alias1,{"name":"isStage","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(5, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + alias4(((helper = (helper = helpers.ProductLink || (depth0 != null ? depth0.ProductLink : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"ProductLink","hash":{},"data":data}) : helper)))
    + "\"><img src=\"/m1/stage/images/responsive-base/missing-image.png\" alt=\""
    + alias4(((helper = (helper = helpers.Name || (depth0 != null ? depth0.Name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"Name","hash":{},"data":data}) : helper)))
    + "\"></a>\n";
},"9":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "            <div class=\"comments\">\n"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : {},(depth0 != null ? depth0.Comments : depth0),{"name":"each","hash":{},"fn":container.program(10, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "            </div>\n";
},"10":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "                    <div class=\"comment\">\n                        <span class='key'>"
    + alias4(((helper = (helper = helpers.Name || (depth0 != null ? depth0.Name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"Name","hash":{},"data":data}) : helper)))
    + ":</span> <span class=\"value\">"
    + alias4(((helper = (helper = helpers.Comment || (depth0 != null ? depth0.Comment : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"Comment","hash":{},"data":data}) : helper)))
    + "</span>\n                    </div>\n";
},"12":function(container,depth0,helpers,partials,data) {
    var stack1, helper;

  return "                "
    + ((stack1 = ((helper = (helper = helpers.ItemPriceSumText || (depth0 != null ? depth0.ItemPriceSumText : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : {},{"name":"ItemPriceSumText","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\n";
},"14":function(container,depth0,helpers,partials,data) {
    var stack1, helper;

  return "                "
    + ((stack1 = ((helper = (helper = helpers.ItemPriceSumNoVatText || (depth0 != null ? depth0.ItemPriceSumNoVatText : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : {},{"name":"ItemPriceSumNoVatText","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\n";
},"16":function(container,depth0,helpers,partials,data) {
    var stack1, helper;

  return "                "
    + ((stack1 = ((helper = (helper = helpers.TotalPriceSumText || (depth0 != null ? depth0.TotalPriceSumText : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : {},{"name":"TotalPriceSumText","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\n";
},"18":function(container,depth0,helpers,partials,data) {
    var stack1, helper;

  return "                "
    + ((stack1 = ((helper = (helper = helpers.TotalPriceSumNoVatText || (depth0 != null ? depth0.TotalPriceSumNoVatText : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : {},{"name":"TotalPriceSumNoVatText","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0 != null ? depth0 : {},(depth0 != null ? depth0.ProductsCart : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"useData":true,"useDepths":true});

this["J"]["views"]["dynamic-cart/dynamic-cart-totals"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper;

  return "                "
    + ((stack1 = ((helper = (helper = helpers.TotalProductSumText || (depth0 != null ? depth0.TotalProductSumText : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : {},{"name":"TotalProductSumText","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\n";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1, helper;

  return "                "
    + ((stack1 = ((helper = (helper = helpers.TotalProductSumNoVatText || (depth0 != null ? depth0.TotalProductSumNoVatText : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : {},{"name":"TotalProductSumNoVatText","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\n";
},"5":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing;

  return "        <div class=\"vat-total\">\n            <span class=\"label\">\n"
    + ((stack1 = helpers["if"].call(alias1,(helpers.isVatIncluded || (depth0 && depth0.isVatIncluded) || alias2).call(alias1,{"name":"isVatIncluded","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(6, data, 0),"inverse":container.program(8, data, 0),"data":data})) != null ? stack1 : "")
    + "            </span>\n            <span class=\"value\">"
    + ((stack1 = ((helper = (helper = helpers.TotalProductVatSumText || (depth0 != null ? depth0.TotalProductVatSumText : depth0)) != null ? helper : alias2),(typeof helper === "function" ? helper.call(alias1,{"name":"TotalProductVatSumText","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</span>\n        </div>\n";
},"6":function(container,depth0,helpers,partials,data) {
    return "                "
    + container.escapeExpression((helpers.translate || (depth0 && depth0.translate) || helpers.helperMissing).call(depth0 != null ? depth0 : {},"dcOfWhichVat",{"name":"translate","hash":{},"data":data}))
    + "\n";
},"8":function(container,depth0,helpers,partials,data) {
    return "                "
    + container.escapeExpression((helpers.translate || (depth0 && depth0.translate) || helpers.helperMissing).call(depth0 != null ? depth0 : {},"dcPlusVat",{"name":"translate","hash":{},"data":data}))
    + "\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing;

  return "<div id=\"dc-totals\">\n    <div class=\"sum-total\">\n        <span class=\"label\">"
    + container.escapeExpression((helpers.translate || (depth0 && depth0.translate) || alias2).call(alias1,"dcItemTotal",{"name":"translate","hash":{},"data":data}))
    + ":</span>\n        <span class=\"value\">\n"
    + ((stack1 = helpers["if"].call(alias1,(helpers.isVatIncluded || (depth0 && depth0.isVatIncluded) || alias2).call(alias1,{"name":"isVatIncluded","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(3, data, 0),"data":data})) != null ? stack1 : "")
    + "        </span>\n    </div>\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.showTotalVat : depth0),{"name":"if","hash":{},"fn":container.program(5, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "</div>\n";
},"useData":true});

this["J"]["views"]["dynamic-cart/dynamic-cart"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing, alias3=container.escapeExpression;

  return "<div id='dc-wrapper' class='reveal-modal' data-reveal>\n    <div id=\"dc-inner\">\n        <h3 id=\"dc-header\">"
    + alias3((helpers.translate || (depth0 && depth0.translate) || alias2).call(alias1,"dcCartHeader",{"name":"translate","hash":{},"data":data}))
    + "</h3>\n        <div id=\"dc-body\">\n            <div id=\"dc-items-header\" class=\"clearfix\">\n                <div class=\"img\">&nbsp;</div>\n                <div class=\"item\">"
    + alias3((helpers.translate || (depth0 && depth0.translate) || alias2).call(alias1,"dcItem",{"name":"translate","hash":{},"data":data}))
    + "</div>\n                <div class=\"price\">"
    + alias3((helpers.translate || (depth0 && depth0.translate) || alias2).call(alias1,"dcPrice",{"name":"translate","hash":{},"data":data}))
    + "</div>\n                <div class=\"qty\">"
    + alias3((helpers.translate || (depth0 && depth0.translate) || alias2).call(alias1,"dcQty",{"name":"translate","hash":{},"data":data}))
    + "</div>\n                <div class=\"delete\">&nbsp;</div>\n                <div class=\"total\">"
    + alias3((helpers.translate || (depth0 && depth0.translate) || alias2).call(alias1,"dcTotal",{"name":"translate","hash":{},"data":data}))
    + "</div>\n            </div>\n            <div id=\"dc-content\">\n                <!-- Cart content goes here, populated from view dynamic-cart-item -->\n            </div>\n            <div id=\"dc-loader-msg\"><span class=\"add-prod\">"
    + alias3((helpers.translate || (depth0 && depth0.translate) || alias2).call(alias1,"dcProdLoadMsg",{"name":"translate","hash":{},"data":data}))
    + "</span></div>\n            <div id=\"dc-totals\">\n            </div>\n        </div>\n        <div id=\"dc-btns\">\n            <a id=\"dc-checkout-btn\" class=\"button\" href=\"/checkout\"><span>"
    + alias3((helpers.translate || (depth0 && depth0.translate) || alias2).call(alias1,"dcCheckoutBtnText",{"name":"translate","hash":{},"data":data}))
    + "</span></a>\n            <a id=\"dc-continue-btn\" class=\"button\" href=\"\"><span>"
    + alias3((helpers.translate || (depth0 && depth0.translate) || alias2).call(alias1,"dcContinueBtnText",{"name":"translate","hash":{},"data":data}))
    + "</span></a>\n        </div>\n    </div>\n</div>\n";
},"useData":true});